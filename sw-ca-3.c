#include <stdio.h>

int main()
{
    int o, a, b, s=1;
    float resFloat = 0;

    
    printf("Ingresa la operación que deseas hacer selecionando el numero:\n (1) suma (2) resta (3) multiplicacion (4) division (5) modulo: ");
    scanf("%d", &o);
    
    printf("Agrega un numero: ");
    scanf("%d", &a);
    
    printf("Agrega otro numero: ");
    scanf("%d", &b);
    
    switch(o){
        case 1:
            b = a + b;
            break;
        case 2:
            b = a - b;
            break;
        case 3:
            b = a * b;
            break;
        case 4:
            if(b!=0){
                resFloat = a / b;
            }else{
                s = 0;
            }
            break;
        case 5:
            if(b!=0){
                b =a % b;
            }else{
                s = 0;
            }
            break;
        default:
            s=0;
    }
    
    if(s==1){
        if(b!=0){
            printf("El resultado es %d", b);
        }else{
            printf("El resultado es %.1f", resFloat);
        }
        
    }else{
        printf("Ups! Has agregado un valor que no es valido");
    }

    return 0;
}