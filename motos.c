#include <stdio.h>

int main()
{
    int marca;
    float precio;
    
    do{
        printf("Marca de la moto: \n (1)Honda (2)Susuki (3)Yamaha (4)Otros \n");
        scanf("%d", &marca);
    }while(marca<0 && marca >5);
    

    printf("Precio de la moto: \n");
    scanf("%f", &precio);
    
    switch(marca){
        case 1:
            precio = precio - precio*0.05;
            break;
        case 2:
            precio = precio - precio*0.1;
            break;
        case 3:
            precio = precio - precio*0.08;
            break;
        case 4:
            precio = precio - precio*0.02;
            break;
    }
    
    printf("El precio con el descuento es de %.2f", precio);
    
    return 0;
}