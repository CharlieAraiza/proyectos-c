#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    char n[5];
    int c, s=1, i= 0, k;
    float sum =0;

    while(n[0]!='W'){
        printf("Escribe un numero: ");
        scanf("%s",n);

        //Contar y validar los caracteres
        while(n[i]!='\0' && s==1){
            s= (n[i]>'9' || n[i]<'0') ? 0 : 1;
            i++;
        }

        //Proceso del programa
       if(s==1){
            k=i;

            //Convertir string a numero
            for(int j = 0; j<i; j++){
                sum = sum + ( (n[j]-48) * pow(10, k-1) );
                k--;
            }

            //Convertir n a g/kg
            printf("Que deseas convertir? (1) kg a g (2) g a kg\n");
            scanf("%d", &c);

            switch(c){
                case 1:
                    printf("%.0f kg son %.0f g", sum, sum*1000);
                break;
                case 2:
                    printf("%.0f g son %.3f kg", sum, sum/1000);
                break;
                default:
                    printf("Ups! No ingresaste una opcion valida.");
            }
       }

        i=0;
        s=1;
        sum=0;
        getch();
        system("cls");
    }

    return 0;
}
