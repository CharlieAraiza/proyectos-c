#include <stdio.h>
#include <stdlib.h>

int main()
{
   int n, par=0, impar=0, sumaPar=0, sumaImpar=0;

   for (int i=0; i<10; i++) {
       printf("Escribe un numero: ");
       scanf("%d", &n);

       if(n%2==0){
           sumaPar = sumaPar + n;
           par++;
       }else{
           sumaImpar = sumaImpar + n;
           impar++;
       }
   }

   float promedio = (float) sumaImpar/impar;

   printf("Cantidad de numeros pares: %d Suma numeros pares: %d, Promedio de numeros impares: %.2f", par, sumaPar, promedio);

    return 0;
}
