#include <stdio.h>


const int F = 5;
const int C =5;

void imprimir_suma(int a[F][C], int n[F][C]);


int main()
{
    int a[F][C], b[F][C];
    
    printf("Agregar los valores de la matriz 1: \n\n");
    for(int i = 0; i<F; i++){
        for(int j= 0; j<C; j++){
            scanf("%d\t", &a[i][j]);
        }
    }
    
    printf("Agregar los valores de la matriz 2: \n\n");
    
    for(int i = 0; i<F; i++){
        for(int j= 0; j<C; j++){
            scanf("%d", &b[i][j]);
        }
    }
    
    //Imprimir la suma de la matriz
    
    imprimir_suma(a, b);

    
    return 0;
}

/*
    arg: a[] es una matriz
    arg: n es el número de elemntos de la matriz 
*/

void imprimir_suma(int m[F][C], int n[F][C]){
    for(int i = 0; i<F; i++){
        for(int j= 0; j<C; j++){
            printf(" %d ", m[i][j] + n[i][j]);
        }printf("\n");
    }
}