#include <stdio.h>

struct empleado{
    char nombre[50];
    float salario;
}empleados[50];

int main()
{  
    
    int n, s = 0, cont = 0;
    float mayor=0, menor=0;
    
    printf("Cuantos empleados deseas agregar? ");
    scanf("%d", &n);
    
    int orden[n];
    
    for(int i = 0; i<n; i++){
        printf("%d Cual es el nombre: ", i+1);
        scanf("%s", empleados[i].nombre);
        
        printf("%d Cual es el salario: ", i+1);
        scanf("%f", &empleados[i].salario);
        
        mayor = (empleados[i].salario>=mayor) ?  empleados[i].salario : mayor;
        menor = (i==0) ?  empleados[i].salario : (empleados[i].salario<=menor) ? empleados[i].salario : menor;
    }

    
    
    for(int i = 0; i<n; i++){
        if(empleados[i].salario == mayor){
            printf("\nNombre: %s \n",empleados[i].nombre);
            printf("Salario: %.2f \n",empleados[i].salario);
        }else if(empleados[i].salario == menor){
            printf("\nNombre: %s \n", empleados[i].nombre);
            printf("Salario: %.2f \n", empleados[i].salario);
        }
    }
    

    return 0;
}