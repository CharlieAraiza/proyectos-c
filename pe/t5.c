
#include <stdio.h>

struct Empleado{
    char nombre[100];
    float valor_hora;
    int antiguedad, horas_mes;
};

int main()
{
    /*Input:    nombre  
                valor_hora
                antiguedad
                horas_mes
    */
    
    
    struct Empleado datos;
    
    printf("Ingresa los datos correspondientes.\nNombre: ");
    scanf("%s", datos.nombre);
    
    printf("Valor hora: ");
    scanf("%f", &datos.valor_hora);
    
    printf("Antiguedad: ");
    scanf("%i", &datos.antiguedad);
    
    printf("Horas trabajadas al mes: ");
    scanf("%d", &datos.horas_mes);
    
    
    /* Proceso: 
                importe = valor_hora*horas_mes + antiguedad*60.5
                importe_neto = importe - importe*0.21
    */
    
    /*Output:   nombre
                antiguedad
                valor_hora
                importe
                importe*0.21
                importe - importe*0.21
    */                    
    
    float importe = (float) datos.valor_hora*datos.horas_mes + datos.antiguedad*60.5;
    
    printf("\n=====================================\n\n");
    printf("Nombre: \t \t \t %s\n", datos.nombre);
    printf("Antiguedad: \t \t \t %d\n", datos.antiguedad);
    printf("Valor hora: \t \t \t %.2f\n", datos.valor_hora);
    printf("Total a cobrar (bruto): \t %.2f\n", importe);
    printf("Impuestos: \t \t \t %.2f\n", importe*0.21);
    printf("\nTotal a cobrar (neto): \t \t %.2f\n", importe - importe*0.21);
    
    
    return 0;
}