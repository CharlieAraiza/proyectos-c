#include <stdio.h>
#include <string.h>

struct corredor{
    char nombre[20];
    int edad;
    char sexo;
    char club[30];
    char caregoria[15];
};

int main()
{
    struct corredor corredor1;

    corredor1.edad = 15;
    
    printf("Ingresa el nombre: ");
    scanf("%s", corredor1.nombre);
    
    printf("Ingresa el sexo (h, m): ");
    scanf(" %c", &corredor1.sexo);
    
    do{
        printf("Ingresa la edad: ");
        scanf(" %d", &corredor1.edad);
    }while(corredor1.edad <=0);

    printf("Ingresa el club: ");
    scanf("%s", corredor1.club);

    strcpy(corredor1.caregoria, (corredor1.edad<=18) ? "juvenil" : (corredor1.edad<=40) ? "senior" : "veterano");

    
    printf("\nNombre: %s \nEdad: %d \nSexo: %c \nCategoria: %s \nClub: %s", corredor1.nombre, corredor1.edad, corredor1.sexo, corredor1.caregoria, corredor1.club);
    
    return 0;
}