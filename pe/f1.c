#include <stdio.h>

void cuadrado();
void rectangulo();
void circulo();
void triangulo();
void trapecio();

int main()
{
    int n;
    
printf("Elige una opción para sacar el area: \n(1) Cuadrado (2) Rectangulo (3) Circulo (4) Triangulo (5) Trapecio\n");
    scanf("%d", &n);
    
    switch(n){
        case 1:
            cuadrado();
            break;
        case 2:
            rectangulo();
            break;
        case 3:
            circulo();
            break;
        case 4:
            triangulo();
            break;
        case 5:
            trapecio();
            break;
        default:
            printf("Ups! Has agregado un valor no valido");
    }
    

    return 0;
}


void cuadrado(){
    float a;
    printf("Escribe el valor del lado: ");
    scanf("%f", &a);
    printf("El área es %.2f", a*a);
}

void rectangulo(){
    float a,b;
    printf("Escribe el valor del primer lado: ");
    scanf("%f", &a);
    printf("Escribe el valor del segundo lado: ");
    scanf("%f", &b);
    printf("El área es %.2f", a*b);
}

void triangulo(){
    float a,b;
    printf("Escribe el valor de la base: ");
    scanf("%f", &a);
    printf("Escribe el valor de la altura: ");
    scanf("%f", &b);
    printf("El área es %.2f", (a*b)/2);
}

void circulo(){
    float a,b;
    printf("Escribe el valor del radio: ");
    scanf("%f", &a);
    printf("El área es %.2f", (a*a*3.14));
}

void trapecio(){
    float a,b,c;
    printf("Escribe el valor de la base menor: ");
    scanf("%f", &a);
    printf("Escribe el valor de la base mayor: ");
    scanf("%f", &b);
    printf("Escribe el valor de la altura: ");
    scanf("%f", &c);
    printf("El área es %.2f", ((b+a)*c)/2);
}