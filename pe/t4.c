#include <stdio.h>

struct alumno{
    char cedula[8], nombre[15], apellido[15], telefono[10];
    int edad;
    float promedio;
}alumnos[100];

int main()
{
    int n, s=0, ref;
    
    printf("Cuantos alumnos deseas agregar? ");
    scanf("%d", &n);
    
    for(int i=0; i<n; i++){
    
        printf("\n");
    
        printf("%d. Ingresa la cedula: ", i+1);
        scanf("%s", alumnos[i].cedula);
        do{
            printf("%d. Ingresa el promedio: ", i+1);
            scanf("%f", &alumnos[i].promedio);  
        }while(alumnos[i].promedio <0 || alumnos[i].promedio>10);

    
    }
    
    int orden[n];
    
    for(int i =0; i<n; i++){
        orden[i] = i;
    }
    
    while(s==0){
        s=1;
        int val;
        
        for(int i = 0; i<n-1; i++){
            
            
            if(alumnos[orden[i]].promedio > alumnos[orden[i+1]].promedio){
                ref = orden[i];
                orden[i] = orden[i+1];
                orden[i+1] = ref;
                s = 0;
            }
        }
    }
    
    printf("\n");
    
    for(int i=n-1; i>=n-2; i--){
        printf("%s %.2f \n", alumnos[orden[i]].cedula, alumnos[orden[i]].promedio);
    }

    
    return 0;
}
