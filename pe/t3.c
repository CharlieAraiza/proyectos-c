#include <stdio.h>

struct alumno{
    char cedula[8], nombre[15], apellido[15], telefono[10];
    int edad;
    float promedio;
}alumnos[100];

int main()
{
    int n;
    
    printf("Cuantos alumnos deseas agregar? ");
    scanf("%d", &n);
    
    for(int i=0; i<n; i++){
    
        printf("\n");
    
        printf("%d. Ingresa la cedula: ", i+1);
        scanf("%s", alumnos[i].cedula);
        
        printf("%d. Ingresa el nombre: ", i+1);
        scanf("%s", alumnos[i].nombre);
        
        printf("%d. Ingresa el apellido: ", i+1);
        scanf("%s", alumnos[i].apellido);
        
        printf("%d. Ingresa la edad: ", i+1);
        scanf("%d", &alumnos[i].edad);
        
        printf("%d. Ingresa el telefono: ", i+1);
        scanf("%s", alumnos[i].telefono);
        
        printf("%d. Ingresa el promedio: ", i+1);
        scanf("%f", &alumnos[i].promedio);
    
    }
    
    
    
    printf("\n=========================\nImprimiendo todos los elementos\n");
    
    for(int i=0; i<n; i++){
        printf("\n");
        printf("%d.- %s\n", i+1, alumnos[i].cedula);
        printf("%d.- %s\n", i+1, alumnos[i].nombre);
        printf("%d.- %s\n", i+1, alumnos[i].apellido);
        printf("%d.- %d\n", i+1, alumnos[i].edad);
        printf("%d.- %s\n", i+1, alumnos[i].telefono);
        printf("%d.- %f\n", i+1, alumnos[i].promedio);
    }
    
    return 0;
}