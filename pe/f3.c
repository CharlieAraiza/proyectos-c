#include <stdio.h>

void ordenado(int m[], int n);

const int N = 3;
int main()
{
    int n[N], ref;
    
    for(int i = 0; i<N; i++){
        printf("%d.- Agrega un valor: ", i+1);
        scanf("%d", &n[i]);
    }

    ordenado(n, N);
    
    return 0;
}

int mayor(int a, int b){
    return (a>b) ? a : b;
}

void ordenado(int m[], int n){
    int s = 0, ref=0;
    while(s==0){
        s=1;
        if(m[0]>m[1]){
            ref =  m[0];
            m[0] = m[1];
            m[1] = ref;
            s = 0;
        }else if(m[1]>m[2]){
            ref =  m[1];
            m[1] = m[2];
            m[2] = ref;
            s = 0;
        }
    }
    
    for(int i = 0; i< n; i++){
        printf("%d ", m[i]);
    }
    
}