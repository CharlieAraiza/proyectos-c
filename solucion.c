#include <stdio.h>

int FIL = 10;
int COL = 10;

int diagonalA(int j, int m[FIL][COL], int f, int c);
int diagonalA2(int j, int m[FIL][COL], int f, int c);

int diagonalB(int j, int m[FIL][COL], int f, int c);
int diagonalB2(int j, int m[FIL][COL], int f, int c);


int vertical(int j, int m[FIL][COL], int f, int c);

int horizontalD(int j, int m[FIL][COL], int f, int c);
int horizontalI(int j, int m[FIL][COL], int f, int c);


int main()
{
    int m[10][10] = {(0,0)}, j = 0;
    
    m[4][3] = 1;
    m[2][3] = 1;
    
    

    int f = 2, c= 4;
    
    int countA = diagonalA(j, m, f, c);
    int countA2 = diagonalA2(j, m, f, c);
    
    int D1 = countA + countA2 - 1;
    
    int countB = diagonalB(j, m, f, c);
    int countB2 = diagonalB2(j, m, f, c);
    
    int D2 = countB + countB2 - 1;
    
    int verticalN = vertical(j, m, f, c);
    
    int horizontalA = horizontalD(j, m, f, c);
    int horizontalB = horizontalI(j, m, f, c);
    
    int Horizontal = horizontalA + horizontalB - 1;
    
    m[f][c] = 5;

        
    for(int i = 0; i<FIL; i++){
        for(int j=0; j<COL; j++){
            printf("%d ", m[i][j]);
        }
        printf("\n");
    }
    
    
    
    printf("F:%d C:%d \nD1: %d D1(b): %d TOTAL: %d", f, c, countA, countA2, D1);
    printf("\nD2: %d D2(b): %d TOTAL: %d\n", countB, countB2, D2);
    printf("H1: %d H2: %d TOTAL: %d \n", horizontalB, horizontalA, Horizontal);
    printf("V TOTAL: %d", verticalN);
    
    return 0;
}



//d = 1 + d(pos+1)
int diagonalA(int j, int m[FIL][COL], int f, int c){
    if(m[f][c]!=j || f==FIL || c ==COL){
        return 0;
    }else{
        return 1+diagonalA(j, m, ++f, ++c);
    }
}

int diagonalA2(int j, int m[FIL][COL], int f, int c){
    if(m[f][c]!=j || f<0 || c<0){
        return 0;
    }else{
        return 1+diagonalA2(j, m, --f, --c);
    }
}


//d = 1 + d(pos+1)
int diagonalB(int j, int m[FIL][COL], int f, int c){
    if(m[f][c]!=j || f<0 || c == COL){
        return 0;
    }else{
        return 1+diagonalB(j, m, --f, ++c);
    }
}

int diagonalB2(int j, int m[FIL][COL], int f, int c){
    if(m[f][c]!=j || f==FIL || c <0){
        return 0;
    }else{
        return 1+diagonalB2(j, m, ++f, --c);
    }
}


//Horizontal

int horizontalD(int j, int m[FIL][COL], int f, int c){
    if(m[f][c]!=j || c==COL){
        return 0;
    }else{
        return 1+horizontalD(j, m, f, ++c);
    }
}

int horizontalI(int j, int m[FIL][COL], int f, int c){
    if(m[f][c]!=j || c<0){
        return 0;
    }else{
        return 1+horizontalI(j, m, f, --c);
    }
}

//Vertical
int vertical(int j, int m[FIL][COL], int f, int c){
    if(m[f][c]!=j || f<0){
        return 0;
    }else{
        return 1 + vertical(j, m, --f, c);
    }
}