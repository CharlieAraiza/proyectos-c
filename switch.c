#include <stdio.h>

int main()
{
    int n, s=1, o, potencia=0;
    char* result;   
    
    do{
        printf("Agrega un numero: ");
        scanf("%d", &n);
        
        printf("Que deseas calcular?: (1)Calcular el cubo del numero. (2)Comrpobar si es par o impar (3)Salir\n");
        scanf("%d", &o);
        
        switch(o){
            case 1:
                    potencia = n;
                    for(int i = 1; i<3; i++){
                        potencia = potencia*n;
                    }
                    printf("El resultado es %d \n", potencia);
                break;
            case 2:
                result = (n%2==0) ? "Es un numero par" : "Es un  numero impar";
                printf("%s \n", result);    
                break;
            case 3:
                s=0;
                break;
            default:
                printf("Ups! has agregado una opción incorrecta.");
        }
        getch();
        
    }while(s==1);

    return 0;
}
