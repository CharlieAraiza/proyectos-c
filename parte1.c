
#include <stdio.h>

int main()
{
   int n, par=0, inpar=0, sumaPar=0, sumaInpar=0;
   
   for (int i=0; i<10; i++) {
       printf("Escribe un numero: ");
       scanf("%d", &n);
       
       if(n%2==0){
           sumaPar = sumaPar + n;
           par++;
       }else{
           sumaInpar = sumaInpar + n;
           inpar++;
       }
   }
   
   float promedio = (float) sumaInpar/inpar; 
   
   printf("Cantidad de numeros pares: %d Suma numeros pares: %d, Promedio de numeros inpares: %.2f", par, sumaPar, promedio);

    return 0;
}
