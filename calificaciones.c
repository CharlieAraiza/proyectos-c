#include <stdio.h>

int main()
{
    /*
        Entrada: 
            -Calificaciones de los 3 primeros parciales
            -Calificación examen final
            -Calificación del trabajo final
    */
    
    
    float n, promedio=0, final=0;
    
    //Calificaciones 3 primeros parciales
    for(int i=0; i<3; i++){
        do{
            printf("Escribe la calificacion del parcial %d: ", i+1);
            scanf("%f", &n);
        }while(n<0 || n>10);
        
        promedio = promedio + n;
    }
    
    final = ((promedio/3)*55)/10;
    
    
    //Calificacion del examen final
    do{
        printf("Escribe la calificacion del examen final: ");
        scanf("%f", &n);
    }while(n<0 || n>10);
    
    final = final + (n*30)/10;
    
    
    //calificacion del trabajo 
    
    do{
        printf("Escribe la calificacion del trabajo final: ");
        scanf("%f", &n);
    }while(n<0 || n>10);
    
    final = final + (n*15)/10;
    
    printf("%.2f", final);


    return 0;
}