#include <stdio.h>
#define YEAR  2019


int main()
{
    int year;
    float salary;
    
    printf("Ingresa el salario: ");
    scanf("%f", &salary);
    
    printf("Anno de ingreso del empleado: ");
    scanf("%d", &year);
    
    if(YEAR-year>=10){
        salary = salary + salary*0.1;
    }else if(YEAR-year<10 && YEAR-year>=5){
        salary = salary + salary*0.07;
    }else if(YEAR-year<5 && YEAR-year>=3){
        salary = salary + salary*0.05;
    }else{
        salary = salary + salary*0.03;
    }
    
    salary = (float) salary - salary*0.16;
    
    printf("Salario mensual con el aumento (con impuestos): %.2f \n Salario anual con el aumento (con impuestos): %.2f\n", salary, salary*12);

    return 0;
}
