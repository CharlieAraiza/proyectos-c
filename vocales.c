#include <stdio.h>

int main()
{   
    char palabra[40], vocalesC[5]={'a','e','i','o','u'}; 
    int  vocales[5] = {0,0,0,0,0}, i=0;
    char l;
    
    printf("Escribe una palabra: ");
    scanf("%s", palabra);
    
    
    while(palabra[i]!='\0'){
        l = palabra[i];
        
        switch(l){
            case 'a':
                vocales[0] = vocales[0] + 1; 
                break;
            
            case 'e':
                vocales[1] = vocales[1] + 1; ;
                break;
            
            case 'i':
                vocales[2] = vocales[2] + 1;
                break;
            
            case 'o':
                vocales[3] = vocales[3] + 1;
                break;
            
            case 'u':
                vocales[4] = vocales[4] + 1;
                break;
        }
        
        i++;
    }
    
    for(int i= 0; i<5; i++){
        printf("%c.- %d", vocalesC[i], vocales[i]);
    }

    return 0;
}