#include <stdio.h>

int check(char n);

int main()
{
    
    char n;
    
    printf("Elige una posición del 0 al 9 ( o 's' para salir): ");
    scanf(" %c", &n);
    
    printf("%d ", check(n));
        

    return 0;
}


int check(char n){
    if(n>=48 && n<=58){
        return n - 48;
    }else if(n==115){
        return -1;
    }
    return 11;
}
