
#include <stdio.h>

int count(int n, int m[5]);
int countleft(int n, int m[5]);

int main()
{
    int array[4] = {0};
    
    array[4] = 1;
    
    for(int i = 0; i<5; i++){
        printf("%d ", array[i]); 
    }
    
    printf("La cantidad de elementos con cero del array es: %d \n", count(0, array));
    printf("La cantidad de elementos con cero del array es: %d \n", countleft(3, array));
    
    return 0;
}

int count(int n, int m[5]){
    if(m[n] != 0 && n<5){
        return 0;
    }else{
        return 1+count(n+1, m);
    }
}

int countleft(int n, int m[5]){
    if( n<=0 && m[n]!=0){
        return 0;
    }else{
        return 1+countleft(n-1, m);
    }
}