
#include <stdio.h>

int diagonalA(int m[5][5], int f, int c);
int diagonalA2(int m[5][5], int f, int c);

int diagonalB(int m[5][5], int f, int c);
int diagonalB2(int m[5][5], int f, int c);


int main()
{
    int m[5][5] = {(0,0)};
    
    m[4][3] = 1;
    m[2][3] = 1;
    
    
    for(int i = 0; i<5; i++){
        for(int j=0; j<5; j++){
            printf("%d ", m[i][j]);
        }
        printf("\n");
    }
    
    int f = 3, c= 3;
    
    int countA = diagonalA(m, f, c);
    int countA2 = diagonalA2(m, f, c);
    
    int D1 = countA + countA2 - 1;
    
    int countB = diagonalB(m, f, c);
    int countB2 = diagonalB2(m, f, c);
    
    int D2 = countB + countB2 - 1;
    
    printf("F:%d C:%d \nD1: %d D1(b): %d TOTAL: %d", f, c, countA, countA2, D1);
    printf("\nD2: %d D2(b): %d TOTAL: %d", countB, countB2, D2);
    
    return 0;
}



//d = 1 + d(pos+1)
int diagonalA(int m[5][5], int f, int c){
    if(m[f][c]!=0 || f==5 || c ==5){
        return 0;
    }else{
        return 1+diagonalA(m, ++f, ++c);
    }
}

int diagonalA2(int m[5][5], int f, int c){
    if(m[f][c]!=0 || f<0 || c<0){
        return 0;
    }else{
        return 1+diagonalA2(m, --f, --c);
    }
}



//d = 1 + d(pos+1)
int diagonalB(int m[5][5], int f, int c){
    if(m[f][c]!=0 || f<0 || c == 5){
        return 0;
    }else{
        return 1+diagonalB(m, --f, ++c);
    }
}

int diagonalB2(int m[5][5], int f, int c){
    if(m[f][c]!=0 || f==5 || c <0){
        return 0;
    }else{
        return 1+diagonalB2(m, ++f, --c);
    }
}