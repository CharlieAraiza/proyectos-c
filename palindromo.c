#include <stdio.h>

int main()
{
    char palabra[50], palabraInvertida[50];
    int i=0, c=0, v=1;
    
    printf("Escribe una palabra (sin espacios): ");
    scanf("%s", palabra);
    
    while(palabra[i]!='\0'){
        i++;    
    }
    
    for(int j=i-1; j>=0; j--){
        palabraInvertida[c] = palabra[j];
        c++;
    }
    
    c=0;
    
    while(v==1 && palabra[c]!='\0'){
        v = (palabra[c]!=palabraInvertida[c]) ? 0 : 1;
        c++;
    }
    
    if(v==1){
        printf("Es un palindromo");
    }else{
        printf("No es un palindromo");
    }
    
    return 0;
}